# Prerequisits
- python3 installed
- root access to install & configure ollama 

# Install
## python env
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
cp ./.env.template ./.env
```

## ollama
### installation
```
sudo curl -fsSL https://ollama.com/install.sh | sh
```
### [OPTIONAL] change model storage folder
By default the models are stored in `/usr/share/ollama/.ollama/models/`

run `sudo systemctl edit ollama.service` to add `OLLAMA_MODELS` as storage path:

```
### Editing /etc/systemd/system/ollama.service.d/override.conf
### Anything between here and the comment below will become the contents of the drop-in file

[Service]
Environment="OLLAMA_MODELS=/local/models/"

### Edits below this comment will be discarded


### /etc/systemd/system/ollama.service
# [Unit]
# Description=Ollama Service
# After=network-online.target
#
# [Service]
# ExecStart=/usr/local/bin/ollama serve
# User=ollama
# Group=ollama
# Restart=always
# RestartSec=3
# Environment="PATH=/usr/local/cuda/bin:/usr/local/bin:/usr/bin:/bin"
#
# [Install]
# WantedBy=default.target
```

run `sudo systemctl deamon-reload && sudo systemctl restart ollama` to reload the config

# Configure
populate the `.env` file with proper information

# Start
```
python3 main.py
```


# Sources
- [LangChain: Run LLMs locally](https://python.langchain.com/docs/guides/development/local_llms/)
- [Ollama](https://ollama.com/)
