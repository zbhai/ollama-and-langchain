import os
import subprocess
from pathlib import Path
# enables counting list items
from operator import length_hint
# .env parser
from dotenv import load_dotenv
# colored print
from colorist import Color, BrightColor, bright_yellow, magenta, red, green
# langchain stuff
from langchain_community.llms import Ollama
from langchain.prompts import PromptTemplate
from langchain.model_laboratory import ModelLaboratory


##################################################
### PREPARING & STARTING
##################################################
green('preparing')
##########
# SET VARS
##########
print('loading environment')
# Load environment variables from .env file
if not os.path.isfile('./.env'):
  raise RuntimeError("Aborting: No .env file found.")
load_dotenv()

##########
# INSTALLING LLMS
##########
# split llm names into array
llmNames = os.environ['LLM_NAMES'].split(',')
green(f'Installing {length_hint(llmNames)} LLMs. This can take a while, depending on installation status, size and bandwith.')
# loop trhough llm names
for idx, llmName in enumerate(llmNames):
  print(f"{idx+1}/{length_hint(llmNames)}: {llmName}")
  # install
  process = subprocess.run(["ollama","pull" , f"{llmName}"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  # on error
  if process.returncode != 0:
    red(process.stderr.decode('utf8'))
    # remove name from array
    llmNames.pop(idx)

# exit if no LLM could be loaded
if length_hint(llmNames) == 0:
  red(f"ABORTING: Unable to install even one of the requested LLMs")
  os._exit(1)

##################################################
### DEFINING THE PROMPT
##################################################
green('preparing the prompt specifications')
# define template
prompt_template = PromptTemplate(
  input_variables=['question'],
  template = """You are a Teacher / Professor. One of your students asks this question. 
  Generate the answer based only on the query. Restrict the answer to the topic. 
  Don't pretend to make a conversation. State the answer clear and precise. The students question is: 
  ---------------------
  {question}
  ---------------------
  """
)

##################################################
### RUNNING THE TEST
##################################################
# define LLMs to run with their specs
localLLMs = []
for idx, llmName in enumerate(llmNames):
  localLLMs.append(Ollama(
    model=llmName,
    temperature=os.environ['OVERAL_TEMPERATURE']
  ))
# setup 'laboratory'
lab = ModelLaboratory.from_llms(localLLMs, prompt=prompt_template)
# run prompt through all llms
lab.compare(os.environ['QUESTION'])